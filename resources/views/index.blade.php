<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Print</title>
	<link href="/css/app.css" rel="stylesheet" type="text/css">
	<link href="/css/style.css" rel="stylesheet" type="text/css">	
	<link href="/css/fontawesome-all.min.css" rel="stylesheet" type="text/css">	
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
</head>
<body>
	
	<div class="flex-center position-ref full-height">
		
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<a class="navbar-brand" href="/">Print</a>
		</nav>
		
		<div class="content">
			<div class="row window">
				
				<div class="col-md-3">
					<div class="card consult">
						<div class="card-header">
							Queries
						</div>
						<div class="card-body queries">
							@foreach($queries as $query)
							<div class="card clickable" onclick="fillExistQuery(this)">
								<div class="card-header header-query">
									<span class="query_id">{{$query->id}}</span>
									<span class="origem">{{$query->origin_zip_code}}</span>
									<i class="fa fa-truck"></i>
									<button type="button" class="close float-right"
										data-toggle="modal" data-target="#deleteQuery"
										onclick="putIdToDeleteQuery(this)">
										<span aria-hidden="true">&times;</span>
									</button>
									<span class="destination">{{$query->origin_zip_code}}</span>
								</div>
								<div class="card-body body-query">									
									<div class="row">
										<div class="col-md-4">
											T: <span class="type">{{$query->volume_type}}</span><br>
											$: <span class="cost">{{$query->cost_of_goods}}</span>
										</div>
										<div class="col-md-4 ">
											Weight: <span class="weight">{{$query->weight}}</span><br>
											Width: <span class="width">{{$query->length}}</span>
										</div>
										<div class="col-md-4 ">
											Height: <span class="height">{{$query->height}}</span><br>
											Length: <span class="length">{{$query->length}}</span>
										</div>
									</div>
									<span class="date-query">
										<i class="far fa-clock"></i> 
										<span class="updated_at">{{$query->updated_at}}</span>
									</span>
								</div>
							</div>
							
							
							@endforeach
							
						</div>
					</div>
				</div>
				
				
				<div class="col-md-4">
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						<strong>Warning!</strong><br> 
						<span class="msgError">Your request has invalid values.</span>
					</div>
					<div class="card">
						<div class="card-header">
							Request Form
						</div>
						
						<input value="{{ csrf_token()}}" type="hidden" name="_token">
						<div class="card-body">
							
							{{ csrf_field() }}
							<input type="text" name="query_id" class="query_id" value="">
							
							<div class="form-group row{{ $errors->has('origin_zip_code') ? ' has-danger' : '' }}">
								
								<label for="origin_zip_code" class="col-sm-5 form-control-label">Origem Code</label>
								<div class="col-sm-7">
									<input id="origin_zip_code" type="text" name="origin_zip_code" placeholder="00000-000" 
									class="form-control {{ $errors->has('origin_zip_code') ? ' is-invalid' : '' }}" 
									oninput="takeOffMsgAndRed(this)">
									
									
									<div class="invalid-feedback origin_zip_code">{{ $errors->first('origin_zip_code') }}</div>
									
								</div>
							</div>
							
							<div class="form-group row{{ $errors->has('destination_zip_code') ? ' has-danger' : '' }}">
								<label for="destination_zip_code" class="col-sm-5 form-control-label">Destiny Code</label>
								<div class="col-sm-7">
									<input id="destination_zip_code" type="text" name="destination_zip_code" placeholder="00000-000" 
									class="form-control {{ $errors->has('destination_zip_code') ? ' is-invalid' : '' }}" 
									oninput="takeOffMsgAndRed(this)">
									<div class="invalid-feedback destination_zip_code">{{ $errors->first('destination_zip_code') }}</div>
								</div>
							</div>
							
							<div class="form-group row{{ $errors->has('weight') ? ' has-danger' : '' }}">
								<label for="weight" class="col-sm-5 form-control-label">Weight</label>
								<div class="col-sm-7">
									<input id="weight" type="text" name="weight" placeholder="00.0" 
									class="form-control {{ $errors->has('weight') ? ' is-invalid' : '' }}" 
									oninput="takeOffMsgAndRed(this)">
									
									<div class="invalid-feedback weight">{{ $errors->first('weight') }}</div>
									
								</div>
							</div>
							
							<div class="form-group row{{ $errors->has('volume_type') ? ' has-danger' : '' }}">
								<label for="volume_type" class="col-sm-5 form-control-label">Volume Type</label>
								<div class="col-sm-7">
									<select name="volume_type" id="volume_type" placeholder="Origem"
									class="form-control {{ $errors->has('volume_type') ? ' is-invalid' : '' }}"
									oninput="takeOffMsgAndRed(this)">
									<option value="box">Box</option>
									<option value="bag">Bag</option>
								</select>
								
								<div class="invalid-feedback volume_type">{{ $errors->first('volume_type') }}</div>
								
							</div>
						</div>
						
						<div class="form-group row{{ $errors->has('cost_of_goods') ? ' has-danger' : '' }}">
							<label for="cost_of_goods" class="col-sm-5 form-control-label">Cost of Goods</label>
							<div class="col-sm-7">
								<input id="cost_of_goods" type="text" name="cost_of_goods" placeholder="000.00" 
								class="form-control {{ $errors->has('cost_of_goods') ? ' is-invalid' : '' }}" 
								oninput="takeOffMsgAndRed(this)">
								
								<div class="invalid-feedback cost_of_goods">{{ $errors->first('cost_of_goods') }}</div>
								
							</div>
						</div>
						
						<div class="form-group row{{ $errors->has('width') ? ' has-danger' : '' }}">
							<label for="width" class="col-sm-5 form-control-label">Width</label>
							<div class="col-sm-7">
								<input id="width" type="text" name="width" placeholder="00" 
								class="form-control {{ $errors->has('width') ? ' is-invalid' : '' }}"  
								oninput="takeOffMsgAndRed(this)">
								
								<div class="invalid-feedback width">{{ $errors->first('width') }}</div>
								
							</div>
						</div>
						
						<div class="form-group row{{ $errors->has('height') ? ' has-danger' : '' }}">
							<label for="height" class="col-sm-5 form-control-label">Height</label>
							<div class="col-sm-7">
								<input id="height" type="text" name="height" placeholder="00" 
								class="form-control {{ $errors->has('height') ? ' is-invalid' : '' }}" 
								oninput="takeOffMsgAndRed(this)">
								<div class="invalid-feedback height">{{ $errors->first('height') }}</div>
							</div>
						</div>
						
						<div class="form-group row">
							<label for="length" class="col-sm-5 form-control-label">Length</label>
							<div class="col-sm-7">
								<input id="length" type="text" name="length" placeholder="00" 
								class="form-control {{ $errors->has('length') ? ' is-invalid' : '' }}" 
								 oninput="takeOffMsgAndRed(this)">									
								<div class="invalid-feedback length"></div>
								
							</div>
						</div>
						
						
					</div>
					<div class="card-footer ">
						<div class="">
							<button class="btn btn-outline-secondary" onclick="cleanFields()">Clean</button>
							<button class="btn btn-outline-secondary" onclick="fillFieldsExemple()">Fill Exemple</button>
							<button class="btn btn-outline-primary float-right" onclick="checkValidation()">Calculate</button>
						</div>
					</div>
					
				</div>
			</div>
			
			
			<div class="col-md-5">					
				<div class="card consult">
					<div class="card-header">
						Deliveries 
						<div class="float-right .mr-2">

							<button class="btn btn-outline-primary btn-sm " 
							data-toggle="modal" data-target="#deliveriesModal"
							onclick="fillDeliveryModalSelected()">Ok</button>
							<button class="btn btn-outline-secondary btn-sm" 							
							onclick="$('.deliveryBody').empty();">Clean</button>
						</div>
					</div>
					<div class="card-body">
						<table class="table table-bordered table-responsible">
							<thead>
								<tr>
									<td></td>
									<th>Estimate Business Day</th>
									<th>Shipping Cost</th>
									<th>Description</th>
									<th></th>
								</tr>
							</thead>
							<tbody class="deliveryBody">								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>		
	</div>
</div>

<div class="modal fade" id="detalhes" tabindex="-1" role="dialog" aria-labelledby="detalhesLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="detalhesLabel">Delivery</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body row">
				<div class="col-md-8">
					<p>Estimate Business Day</p>
					<p>Shipping Cost</p>
					<p>Description</p>
				</div>
				<div class="col-md-4">
					<p class="days float-right"></p>
					<p class="cost float-right"></p>
					<p class="description float-right"></p>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="deliveriesModal" tabindex="-1" role="dialog" 
		aria-labelledby="detalhesLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="detalhesLabel">Deliveries</h5>
				<button type="button" class="close" data-dismiss="modal" 
						aria-label="Close" onclick="cleanDeliveries()">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body deliveriesModal">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-outline-secondary" data-dismiss="modal"
					onclick="cleanDeliveries()">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="deleteQuery" tabindex="-1" role="dialog" 
		aria-labelledby="detalhesLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="detalhesLabel">Delete Query?</h5>
				<button type="button" class="close" data-dismiss="modal" 
						aria-label="Close" onclick="cleanDeliveries()">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Are you sure you want to delete this query?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-outline-secondary" data-dismiss="modal"
					onclick="cleanDeliveries()">Close</button>
				<button class="btn btn-outline-primary float-right" data-dismiss="modal" 
					onclick="deleteQuery(this)">Delete</button>
			</div>
		</div>
	</div>
</div>

<script src="/js/jquery.min.js"></script>
<script src="/js/app.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/functions.js"></script>
</body>
</html>
