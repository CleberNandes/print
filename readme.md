## Instalando o Projeto Print

requisitos
- php 7
- composer
- mysql

clone o projeto
$ git clone https://CleberNandes@bitbucket.org/CleberNandes/print.git

entre na pasta print
$ cd print

atualize as dependências necessárias ao projeto
$ composer update

copie e renomeie o arquivo no diretório raiz ".env.example" para ".env"

crie um banco de dados chamado "print" no mysql

mude as informações de usuario e senha no arquivo ".env" usadas no seu banco de dados
DB_USERNAME=homestead
DB_PASSWORD=secret

caso queira usar outro banco de dados mude o ".env" para o escolhido
DB_CONNECTION=mysql

rode o comando no terminal dentro da pasta /print
$ php artisan migrate

suba o servidor php laravel
$ php artisan serve

Fiz uma funcionalidade a mais, salvando, ou se existir, atualizando uma consulta.




