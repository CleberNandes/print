<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QueryRequest extends FormRequest
{
    /**
    * Determine if the user is authorized to make this request.
    *
    * @return bool
    */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'origin_zip_code'          => 'required|size:9',
            'destination_zip_code'     => 'required|size:9',
            'weight'                   => 'required|numeric',
            'volume_type'              => 'required|string|max:20',
            'cost_of_goods'            => 'required|numeric|min:1',
            'width'                    => 'required|integer|min:1',
            'height'                   => 'required|integer|min:1',
            'length'                   => 'required|integer|min:1'
        ];
    }

}