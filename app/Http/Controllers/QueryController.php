<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Query;
use App\Delivery;
use App\Http\Requests\QueryRequest;
use GuzzleHttp\Psr7\Request as GuzzleRequest;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Client;
use HttpRequest;

class QueryController extends Controller
{
	public function index(){
		$queries = Query::orderBy('updated_at','Desc')->get();
		$deliveries = Delivery::all();
		$query = new Query;
		return view('index', compact('queries','deliveries','query'));
	}
	
	public function create(QueryRequest $req){

		$exist = true;
		$query = Query::find($req->query_id);
		if($query == null){
			$exist = false;
			$query = new Query;
		}
		$query->fill($req->all());
		$query->save();

		return response()->json(['exist' => $exist,'query' => $query]);
	}	

	public function delete($id){
		Query::destroy($id);
		return response()->json(['delete'=>true,'id'=>$id]);
	}
}
		