<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    protected $table = "deliveries";

    protected $fillable = [
        'query_id',
        'delivery_estimate_business_days',
        'final_shipping_cost',
        'description'
    ];

    /* public function query(){
        //return $this->belongsTo('App\Query');
    } */
}
