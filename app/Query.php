<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Query extends Model
{
    protected $table = 'queries';

    protected $fillable = [
        'origin_zip_code',    
        'destination_zip_code',     
        'weight',
        'volume_type',
        'cost_of_goods',
        'width',
        'height',
        'length'
    ];
}
