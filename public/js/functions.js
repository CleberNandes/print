function cleanFields() {
	$("#origin_zip_code").val("");
	$("#destination_zip_code").val("");
	$("#weight").val("");
	$("#cost_of_goods").val("");
	$("#width").val("");
	$("#height").val("");
	$("#length").val("");
	$(".query_id").val("");
}

function takeOffAllMsgAndRed(){
	$('.card').find('.is-invalid').removeClass("is-invalid");
  $('.card').find(".invalid-feedback").text("");
}

function fillFieldsExemple() {
	var data = {
		origin_zip_code: "04012-090",
		destination_zip_code: "04037-003",
		weight: 0.5,
		volume_type: "bag",
		cost_of_goods: 100,
		width: 10,
		height: 10,
		length: 25,
		query_id: "1"
	};
	fillFields(data); 
	takeOffAllMsgAndRed();
}

function deleteQuery(ele){
	var id = $(ele).parents('.modal').attr('query_id');
	
	$.ajax({
    url: `/delete/${id}`,
    type: "delete",
    headers: {
      "X-CSRF-Token": $('[name="_token"]').val()
    },
    success: function(event) {
      console.log("Dados foram validados!");
	  //take off query from card if deleted
	  console.log(event);
	  if(event.delete){
		  takeQueryOff(event.id);
	  }
    },
    error: function(json) {
      console.log(json.responseJSON)
    }
  });
}

function putIdToDeleteQuery(ele){
	var id = $(ele).parents('.header-query').find('.query_id').text();
	$(ele).parents('.clickable').addClass('query_id_'+id);
	$('#deleteQuery').attr('query_id',id);
}

function takeQueryOff(id){
	$('.clickable.query_id_'+id).remove();
}

function fillExistQuery(ele){
	//get data from query
	var data = {
		origin_zip_code: $(ele).find('.origem').text(),
		destination_zip_code: $(ele).find('.destination').text(),
		weight: $(ele).find('.weight').text(),
		volume_type: $(ele).find('.type').text(),
		cost_of_goods: $(ele).find('.cost').text(),
		width: $(ele).find('.width').text(),
		height: $(ele).find('.height').text(),
		length: $(ele).find('.length').text(),
		query_id: $(ele).find('.query_id').text()
	};
	fillFields(data); 
	takeOffAllMsgAndRed();
}


function insertNewQueryOnCard(data){

	//put data on form
	var query = `
		<div class="card clickable" onclick="fillExistQuery(this)">
			<div class="card-header header-query">
				<span class="query_id">${data.query_id}</span>
				<span class="origem">${data.origin_zip_code}</span>
				<i class="fa fa-truck"></i>
				<button type="button" class="close float-right"
					data-toggle="modal" data-target="#deleteQuery">
					<span aria-hidden="true">&times;</span>
				</button>
				<span class="destination">${data.origin_zip_code}</span>
			</div>
			<div class="card-body body-query">									
				<div class="row">
					<div class="col-md-4">
						T: <span class="type">${data.volume_type}</span><br>
						$: <span class="cost">${data.cost_of_goods}</span>
					</div>
					<div class="col-md-4 ">
						Weight: <span class="weight">${data.weight}</span><br>
						Width: <span class="width">${data.length}</span>
					</div>
					<div class="col-md-4 ">
						Height: <span class="height">${data.height}</span><br>
						Length: <span class="length">${data.length}</span>
					</div>
				</div>
				<span class="date-query">
					<i class="far fa-clock"></i> 
					<span class="updated_at">${data.updated_at}</span>
				</span>
			</div>
		</div>
	`;
	$(".consult .queries").append(query);

	
}

function fillFields(data) {
	$("#origin_zip_code").val(data.origin_zip_code);
	$("#destination_zip_code").val(data.destination_zip_code);
	$("#weight").val(data.weight);
	$("#volume_type").val(data.volume_type);
	$("#cost_of_goods").val(data.cost_of_goods);
	$("#width").val(data.width);
	$("#height").val(data.height);
	$("#length").val(data.length);
	$(".query_id").val(data.query_id);
}

function fillDeliveryModal(ele) {
	var days = $(ele).parents('tr').find("td:nth-child(2)").text();
	var cost = $(ele).parents('tr').find("td:nth-child(3)").text();
	var description = $(ele).parents('tr').find("td:nth-child(4)").text();
	$(".days").text(days);
	$(".cost").text(cost);
	$(".description").text(description);
}

function takeOffMsgAndRed(ele) {
	$(ele).removeClass("is-invalid");
	$(ele).parent().find(".invalid-feedback").text("");
	$(ele).parents("card-body").find('.query_id').val("");
}

function checkValidation() {
	$(".deliveryBody").empty();
	var origin = $("#origin_zip_code").val();
	var destination = $("#destination_zip_code").val();
	var weight = $("#weight").val();
	var type = $("#volume_type").val();
	var cost = $("#cost_of_goods").val();
	var width = $("#width").val();
	var height = $("#height").val();
	var length = $("#length").val();
	var id = $(".query_id").val();
	
	var data = {
		origin_zip_code: origin,
		destination_zip_code: destination,
		weight: weight,
		volume_type: type,
		cost_of_goods: cost,
		width: width,
		height: height,
		length: length,
		query_id: id
	};
	
	$.ajax({
		url: "/create",
		data: data,
		type: "POST",
		headers: {
			"X-CSRF-Token": $('[name="_token"]').val()
		},
		success: function(event) {
			console.log("Dados foram validados!");
			//send to external api
			sendApi(data);
			console.log(event);
			if(!event.exist){
				//put new query on queries card
				insertNewQueryOnCard(event.query);
			}
		},
		error: function(json) {
			$.each(json.responseJSON.errors, function(i, v) {
				$(`.form-group .${i}`).text(v);
				$(`#${i}`).addClass("is-invalid");
			});
		}
	});
}

function sendApi(data) {
	var body = `{
		"origin_zip_code":"${data.origin_zip_code}",
		"destination_zip_code":"${data.destination_zip_code}",
		"volumes":[{
			"weight":${data.weight},
			"volume_type":"${data.volume_type}",
			"cost_of_goods":${data.cost_of_goods},
			"width":${data.width},
			"height":${data.height},
			"length":${data.length}
		}]
	}`;
	
	$.ajax({
		data: body,
		url: "https://api.intelipost.com.br/api/v1/quote",
		method: "POST",
		headers: {
			"content-Type": "application/json",
			platform: "intelipost-docs",
			"api-key":
			"9009f95101bf48b01a50928a2a71ed1ae9083fc1d3c08439b0613dfc38e656c5"
		},
		dataType: "json",
		crossDomain: true,
		async: true,
		processData: false,
		success: function(event) {
			if (event.content.delivery_options.lenght == 0) {
				$(".msgError").text("Nenhum resultado retornou da sua pesquisa");
				$(".alert").show();
			}
			buildDeliveryTable(event.content.delivery_options);
			cleanFields();

		},
		error: function(json) {
			var msg = "";
			$.each(json.responseJSON.messages, function(i, v) {
				msg += v.text;
			});
			$(".msgError").text(msg);
			$(".alert").show();
		}
	});
}

function buildDeliveryTable(deliveries) {
	$.each(deliveries, function(i, v) {
		var delivery = `
			<tr>
			<td><input type="checkbox" name="select"/></td>
			<td>${v.delivery_estimate_business_days}</td>
			<td>${v.final_shipping_cost}</td>
			<td>${v.description}</td>
			<td><button class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#detalhes" onclick="fillDeliveryModal(this)">Ok</button></td>
			</tr>
		`;
		$(".deliveryBody").append(delivery);
	});
}

function fillDeliveryModalSelected() {
	var deliveries = $(".deliveryBody tr td:first-child input:checked");
	
	$.each(deliveries,function(i) {
		var day = $(this).parents('tr').find('td:nth-child(2)').text();
		var cost = $(this).parents('tr').find('td:nth-child(3)').text();
		var desc = $(this).parents('tr').find('td:nth-child(4)').text();
		var qtde = "day";
		if(day > 1){
			qtde = "days";
		}
		var delivery = `<p>${desc}, ${cost} $, ${day} ${qtde}</p>`;
		$(".deliveriesModal").append(delivery);

		
	});
}

function cleanDeliveries(){
	$(".deliveriesModal").empty();
}
